from golang as builder

WORKDIR /app
ADD . .
RUN go build -o /usr/local/bin/myfood-app

EXPOSE 4002
CMD ["/usr/local/bin/myfood-app"]
