package config

import (
	"github.com/harranali/authority"
)

func AuthorityInit() *authority.Authority {

	auth := authority.New(authority.Options{
		TablesPrefix: "authority_",
		DB:           DB,
	})

	return auth
}
