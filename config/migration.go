package config

import (
	"github.com/harranali/authority"
)

func AuthorityMigration() {
	authority.New(authority.Options{
		TablesPrefix: "authority_",
		DB:           DB,
	})
}
