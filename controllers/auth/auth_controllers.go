package controllers

import (
	"golang-mini-project/entity"
	"golang-mini-project/helper"
	"golang-mini-project/model"
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

func Authentication(ctx echo.Context) error {
	auth := new(entity.RequestAuth)

	if err := ctx.Bind(auth); err != nil {
		return ctx.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  http.StatusBadRequest,
			"message": "Status bad request,",
		})
	}

	validate := validator.New()

	validatorErrs := validate.Struct(auth)

	if validatorErrs != nil {
		return ctx.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  http.StatusBadRequest,
			"message": validatorErrs.Error(),
		})
	}

	username := auth.Username
	password := auth.Password

	check, dbpassword := model.CheckAuth(username, password)

	if !check {
		return echo.ErrUnauthorized
	}

	if dbpassword == "nil" {
		return echo.ErrUnauthorized
	}

	match, _ := helper.CheckPasswordHash(password, dbpassword)

	if !match {
		return echo.ErrUnauthorized
	}

	// Set custom claims
	claims := &entity.JwtCustomClaims{
		username,
		true,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	/**
	// hash, _ := helper.HashPassword(password)
		Disini dimasukin checking username ke basis data
	*/

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	secretKey := helper.GetEnvVariables("JWT_SECRETKEY")

	tokenAccess, err := token.SignedString([]byte(secretKey))
	if err != nil {
		return err
	}

	/**
	Using logrus for logging

	hasil := log.WithFields(log.Fields{
		"method": ctx.Request().Method,
	})

	fmt.Println(hasil)
	*/

	return ctx.JSON(http.StatusOK, echo.Map{
		"status": http.StatusOK,
		"token":  tokenAccess,
		"data":   auth,
	})
}
