package controllers

import (
	"golang-mini-project/entity"
	"golang-mini-project/helper"
	"golang-mini-project/model"
	"io"
	"net/http"
	"os"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

func FindAllProduct(ctx echo.Context) error {
	product, err := model.FindAllProduct(ctx)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"status":  http.StatusBadRequest,
			"error":   1,
			"message": err.Error(),
		})
	}

	return ctx.JSON(http.StatusOK, echo.Map{
		"status":  http.StatusOK,
		"error":   1,
		"message": "Get data successfully",
		"data":    product,
	})
}

func FindById(ctx echo.Context) error {
	id := ctx.Param("id")
	product, err := model.FindById(id)

	if err != nil {
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"status":  http.StatusBadRequest,
			"error":   1,
			"message": err.Error(),
		})
	}

	return ctx.JSON(http.StatusOK, echo.Map{
		"status":  http.StatusOK,
		"error":   1,
		"message": "Get data successfully",
		"data":    product,
	})
}

/**
@author MaulanaMuhammadRizky
Parameter pada struct request product
*/
func SaveProduct(ctx echo.Context) error {

	requestProduct := new(entity.RequestProduct)

	product_name := ctx.FormValue("product_name")
	product_price, _ := strconv.Atoi(ctx.FormValue("product_price"))
	product_qty, _ := strconv.Atoi(ctx.FormValue("product_qty"))
	file, errFile := ctx.FormFile("file")

	requestProduct.ProductName = product_name
	requestProduct.ProductPrice = uint64(product_price)
	requestProduct.ProductQty = uint16(product_qty)
	requestProduct.ProductLink = file.Filename

	if err := ctx.Bind(requestProduct); err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	validate := validator.New()

	validationErrors := validate.Struct(requestProduct)

	if validationErrors != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, validationErrors.Error(), ctx)
	}

	/** Uploading files */

	if errFile != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, errFile.Error(), ctx)
	}

	src, errSrc := file.Open()
	if errSrc != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, errSrc.Error(), ctx)
	}
	defer src.Close()

	// Destination
	filename := "uploads/" + file.Filename
	dst, errDest := os.Create(filename)
	if errDest != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, errDest.Error(), ctx)
	}

	defer dst.Close()

	// Copy
	if _, errCopy := io.Copy(dst, src); errCopy != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, errCopy.Error(), ctx)
	}

	/** End of uploading */

	responseErr := model.SaveProduct(requestProduct)

	if responseErr != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, responseErr.Error(), ctx)
	}

	return helper.ResponseStatus(http.StatusOK, 0, "Create data successfully", ctx)

}

func DeleteProduct(ctx echo.Context) error {
	id := ctx.Param("id")

	_, err := model.DeleteProduct(id)

	if err != nil {

		return ctx.JSON(http.StatusOK, echo.Map{
			"status":  http.StatusOK,
			"error":   1,
			"message": err.Error(),
		})
	}

	return ctx.JSON(http.StatusOK, echo.Map{
		"status":  http.StatusOK,
		"error":   1,
		"message": "Delete product successfully",
	})

}
