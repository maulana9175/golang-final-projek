package controllers

import (
	"golang-mini-project/helper"
	"golang-mini-project/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func FindAllRole(ctx echo.Context) error {

	role, err := model.FindAllRole()

	if err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	return helper.ResponseSuccess(http.StatusOK, 1, "Get data successfully,", ctx, role)
}

func FindRoleById(ctx echo.Context) error {
	id := ctx.Param("id")

	role, err := model.FindRoleById(id)

	if err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	return helper.ResponseSuccess(http.StatusOK, 1, "Get data successfully,", ctx, role)

}

func SaveRole(ctx echo.Context) error {

	return ctx.JSON(http.StatusOK, nil)
}

func DeleteRoleById(ctx echo.Context) error {

	return ctx.JSON(http.StatusOK, nil)
}
