package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func FindAllTransaksi(ctx echo.Context) error {

	return ctx.JSON(http.StatusOK, echo.Map{
		"status":  http.StatusOK,
		"message": "Get data successfully",
	})
}
