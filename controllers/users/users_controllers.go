package controllers

import (
	"golang-mini-project/entity"
	"golang-mini-project/helper"
	"golang-mini-project/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func FindAllUser(ctx echo.Context) error {

	return ctx.JSON(http.StatusOK, nil)
}

func SaveUser(ctx echo.Context) error {
	requestUser := new(entity.Requestuser)
	user := new(entity.Users)

	if err := ctx.Bind(&requestUser); err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}

	hashPassword, _ := helper.HashPassword(requestUser.Password)

	user.Username = requestUser.Username
	user.Password = hashPassword
	user.Name = requestUser.FullName

	_, err := model.SaveUser(user)

	if err != nil {
		return echo.ErrBadRequest
	}

	return helper.ResponseStatus(http.StatusOK, 1, "Create user successfully", ctx)

}
