package controllers

import (
	"net/http"

	"golang-mini-project/entity"
	"golang-mini-project/helper"
	"golang-mini-project/model"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

func FindAllVendor(ctx echo.Context) error {

	result, err := model.FindAllVendor(ctx)

	if err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	return ctx.JSON(http.StatusOK, echo.Map{
		"status":  http.StatusOK,
		"error":   0,
		"message": "Get data successfully",
		"data":    result,
	})
}

func FindVendorById(ctx echo.Context) error {
	id := ctx.Param("id")

	result, err := model.FindVendorById(id)

	if err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	return ctx.JSON(http.StatusOK, echo.Map{
		"status":  http.StatusOK,
		"error":   0,
		"message": "Get data successfully",
		"data":    result,
	})
}

func SaveVendor(ctx echo.Context) error {
	requestVendor := new(entity.RequestVendor)

	if err := ctx.Bind(requestVendor); err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	validate := validator.New()

	validationErrors := validate.Struct(requestVendor)

	if validationErrors != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, validationErrors.Error(), ctx)
	}

	_, err := model.SaveVendor(requestVendor)

	if err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	return helper.ResponseStatus(http.StatusOK, 1, "Create data successfully", ctx)

}

func DeleteVendor(ctx echo.Context) error {
	id := ctx.Param("id")

	_, errFind := model.FindVendorById(id)

	if errFind != nil {
		return helper.ResponseStatus(http.StatusNotFound, 1, errFind.Error(), ctx)
	}

	_, err := model.DeleteVendor(id)

	if err != nil {
		return helper.ResponseStatus(http.StatusBadRequest, 1, err.Error(), ctx)
	}

	return helper.ResponseStatus(http.StatusOK, 0, "Delete data successfully", ctx)

}
