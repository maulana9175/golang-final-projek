package entity

type RequestAuth struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

func (RequestAuth) TableName() string {
	return "users"
}
