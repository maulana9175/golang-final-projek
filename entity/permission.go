package entity

type Permission struct {
	ID   int `gorm:"primaryKey"`
	Name string
}
