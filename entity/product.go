package entity

import "time"

type Products struct {
	ProductId int       `gorm:"primaryKey"`
	Nama      string    `json:"product_name"`
	Price     int       `json:"product_price"`
	Qty       int       `json:"product_qty"`
	Image     string    `json:"product_image"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ReponseProduct struct {
	ProductId int       `gorm:"primaryKey" json:"product_id"`
	Nama      string    `json:"product_name"`
	Price     uint64    `json:"product_price"`
	Qty       uint16    `json:"product_qty"`
	CreatedAt time.Time `json:"created_at"`
}

type RequestProduct struct {
	ProductName  string `json:"product_name" validate:"required"`
	ProductPrice uint64 `json:"product_price" validate:"required"`
	ProductQty   uint16 `json:"product_qty" validate:"required"`
	ProductLink  string
}
