package entity

type Role struct {
	ID   int `gorm:"primaryKey"`
	Name string
}

type UserRole struct {
	ID     int `gorm:"primaryKey"`
	UserId int
	RoleId int
}

func (Role) TableName() string {
	return "authority_roles"
}

func (UserRole) TableName() string {
	return "authority_user_roles"
}
