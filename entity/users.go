package entity

type Users struct {
	Username string
	Password string
	Name     string
}

type ResponseUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

type Requestuser struct {
	Username string `json:"username"`
	Password string `json:"password"`
	FullName string `json:"full_name"`
}
