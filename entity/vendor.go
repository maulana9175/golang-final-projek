package entity

type Vendor struct {
	VendorId     uint `gorm:"primaryKey"`
	VendorName   string
	VendorDetail VendorDetail `gorm:"foreignKey:VendorId;references:VendorId"`
}

type VendorDetail struct {
	ID       uint   `json:"vendor_detail_id"`
	VendorId uint   `json:"vendor_id"`
	Alamat   string `json:"vendor_alamat"`
	Telepon  string `json:"vendor_telepon"`
	Email    string `json:"vendor_email"`
}

type ResponseVendor struct {
	VendorName   string      `json:"vendor_name"`
	VendorDetail interface{} `json:"vendor_detail"`
}

type RequestVendor struct {
	VendorName    string `json:"vendor_name" validate:"required"`
	VendorAlamat  string `json:"vendor_alamat" validate:"required"`
	VendorTelepon string `json:"vendor_telepon" validate:"required"`
	VendorEmail   string `json:"vendor_email" validate:"required"`
}

func (VendorDetail) TableName() string {
	return "vendor_detail"
}
