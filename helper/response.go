package helper

import (
	"github.com/labstack/echo/v4"
)

type Response struct {
	ErrorCode int
	Message   string
	Data      interface{}
}

func ResponseStatus(status, errorCode int, message interface{}, ctx echo.Context) error {
	return ctx.JSON(status, echo.Map{
		"status":  status,
		"error":   errorCode,
		"message": message,
	})
}

func ResponseSuccess(status, errorCode int, message string, ctx echo.Context, data interface{}) error {
	return ctx.JSON(status, echo.Map{
		"status":  status,
		"error":   errorCode,
		"message": message,
		"data":    data,
	})
}
