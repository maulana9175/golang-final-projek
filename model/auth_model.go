package model

import (
	"golang-mini-project/config"
	"golang-mini-project/entity"
)

func CheckAuth(username, password string) (check bool, hashpassword string) {
	var auth entity.RequestAuth

	tx := config.DB.Where("username = ?", username).First(&auth)

	if tx.Error != nil {
		return false, "nil"
	}

	return true, auth.Password
}
