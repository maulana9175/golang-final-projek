package model

import (
	"golang-mini-project/config"
	"golang-mini-project/entity"
)

func FindAllPermission() ([]entity.Permission, error) {
	var permission []entity.Permission
	tx := config.DB.Find(&permission)

	if tx.Error != nil {
		return nil, tx.Error
	}

	return permission, nil
}

func FindPermissionById(Id string) (entity.Permission, error) {
	var permission entity.Permission

	tx := config.DB.First(&permission)

	if tx.Error != nil {
		return permission, tx.Error
	}

	return permission, nil
}
