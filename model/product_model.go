package model

import (
	"golang-mini-project/config"
	"golang-mini-project/entity"

	"github.com/labstack/echo/v4"
)

func FindAllProduct(ctx echo.Context) ([]entity.ReponseProduct, error) {
	var Products []entity.ReponseProduct

	tx := config.DB.Debug().Raw("SELECT product_id, nama, price, qty, created_at FROM products").Scan(&Products)

	if tx.Error != nil {
		return nil, tx.Error
	}

	return Products, nil
}

func FindById(Id string) (entity.Products, error) {
	var products entity.Products

	tx := config.DB.Debug().First(&products, Id)
	if tx.Error != nil {
		return products, tx.Error
	}

	return products, nil
}

func SaveProduct(requestProduct *entity.RequestProduct) error {

	var products entity.Products
	products.Nama = requestProduct.ProductName
	products.Price = int(requestProduct.ProductPrice)
	products.Qty = int(requestProduct.ProductQty)
	products.Image = requestProduct.ProductLink

	tx := config.DB.Create(&products)

	if tx.Error != nil {
		return tx.Error
	}

	return nil
}

func DeleteProduct(ID string) (bool, error) {
	var products entity.Products

	tx := config.DB.Delete(&products, ID)

	if tx.Error != nil {
		return false, tx.Error
	}

	return true, nil
}
