package model

import (
	"golang-mini-project/config"
	"golang-mini-project/entity"
)

func FindAllRole() ([]entity.Role, error) {
	var role []entity.Role
	tx := config.DB.Find(&role)

	if tx.Error != nil {
		return nil, tx.Error
	}

	return role, nil
}

func FindRoleById(Id string) (entity.Role, error) {
	var role entity.Role
	tx := config.DB.First(&role, Id)

	if tx.Error != nil {
		return role, tx.Error
	}

	return role, nil
}

func DeleteRole(Id string) (bool, error) {
	var role entity.Role
	tx := config.DB.Delete(&role, Id)

	if tx.Error != nil {
		return false, tx.Error
	}

	return true, nil
}
