package model

import (
	"golang-mini-project/config"
	"golang-mini-project/entity"
)

func FindAllModel() ([]entity.Users, error) {
	var result []entity.Users

	tx := config.DB.Find(&result)

	if tx.Error != nil {
		return nil, tx.Error
	}

	return result, nil

}

func SaveUser(request *entity.Users) (bool, error) {

	tx := config.DB.Create(&request)

	if tx.Error != nil {
		return false, tx.Error
	}

	return true, nil
}
