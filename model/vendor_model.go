package model

import (
	"golang-mini-project/config"
	"golang-mini-project/entity"
	"strconv"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func FindAllVendor(ctx echo.Context) ([]entity.Vendor, error) {
	var vendor []entity.Vendor
	tx := config.DB.Scopes(Paginate(ctx)).Find(&vendor)

	if tx == nil {
		return vendor, tx.Error
	}

	return vendor, nil
}

func FindVendorById(Id string) (entity.Vendor, error) {
	var vendor entity.Vendor

	tx := config.DB.Debug().First(&vendor, Id)
	if tx.Error != nil {
		return vendor, tx.Error
	}

	return vendor, nil
}

func SaveVendor(requestVendor *entity.RequestVendor) (bool, error) {
	// requestProduct := new(entity.RequestProduct)

	var vendor entity.Vendor
	var vendordetail entity.VendorDetail

	vendor.VendorName = requestVendor.VendorName

	tx := config.DB.Begin()

	err := tx.Create(&vendor).Error

	if err != nil {
		tx.Rollback()
		return false, err
	}
	vendordetail.Alamat = requestVendor.VendorAlamat
	vendordetail.Email = requestVendor.VendorEmail
	vendordetail.Telepon = requestVendor.VendorTelepon
	vendordetail.VendorId = vendor.VendorId

	errdetail := tx.Create(&vendordetail).Error

	if errdetail != nil {
		tx.Rollback()
		return false, errdetail
	}

	tx.Commit()

	return true, nil
}

func DeleteVendor(Id string) (bool, error) {
	var vendor entity.Vendor

	tx := config.DB.Delete(&vendor, Id)

	if tx.Error != nil {
		return false, tx.Error
	}

	return true, nil
}

func Paginate(r echo.Context) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		q := r.Request().URL.Query()
		page, _ := strconv.Atoi(q.Get("page"))
		if page == 0 {
			page = 1
		}

		pageSize, _ := strconv.Atoi(q.Get("page_size"))
		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 10
		}

		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}
