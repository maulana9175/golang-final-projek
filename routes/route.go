package routes

import (
	"golang-mini-project/authmiddleware"
	authentication "golang-mini-project/controllers/auth"
	product "golang-mini-project/controllers/product"
	controllers "golang-mini-project/controllers/role"
	users "golang-mini-project/controllers/users"
	vendor "golang-mini-project/controllers/vendor"

	"github.com/labstack/echo/v4"
)

func RouteInit(route echo.Echo) {
	route.GET("/products", product.FindAllProduct, authmiddleware.IsAuthenticated)
	route.GET("/products/:id", product.FindById, authmiddleware.IsAuthenticated)
	route.POST("/products", product.SaveProduct, authmiddleware.IsAuthenticated)
	route.DELETE("/products/:id", product.DeleteProduct, authmiddleware.IsAuthenticated)

	route.GET("/vendors", vendor.FindAllVendor)
	route.GET("/vendors/:id", vendor.FindVendorById)
	route.POST("/vendors", vendor.SaveVendor)
	route.DELETE("/vendors/:id", vendor.DeleteVendor)

	role := route.Group("role")
	auth := route.Group("auth")
	user := route.Group("user")

	role.GET("/roles", controllers.FindAllRole)
	role.GET("/roles/:id", controllers.FindRoleById)
	role.POST("/roles", controllers.SaveRole)
	role.DELETE("/roles/:id", controllers.DeleteRoleById)

	auth.POST("/authentication", authentication.Authentication)

	user.GET("/users", users.FindAllUser)
	user.POST("/users", users.SaveUser)

}
