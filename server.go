package main

import (
	"golang-mini-project/config"
	"golang-mini-project/routes"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()

	// migration.MaulanaIni()
	config.DatabaseConnection()
	config.AuthorityMigration()

	routes.RouteInit(*e)

	e.Logger.Fatal(e.Start(":7000"))
}
